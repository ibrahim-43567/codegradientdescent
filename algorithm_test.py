import inspect
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation


def test_algorithms(algorithms):
    def parabola_1(x):
        return x[0]**2 + x[1]**2

    def grad_parabola_1(x):
        return np.array([2*x[0], 2*x[1]])

    def hessian_parabola_1(x):
        return np.array([[2., 0.], [0., 2.]])

    x0_parabola_1 = np.array([-1., 1.5])
    mins_parabola_1 = [np.array([0., 0.]), ]


    def parabola_2(x):
        return 2.*x[0]**2 + 1.5*x[1]**2

    def grad_parabola_2(x):
        return np.array([4*x[0], 3*x[1]])

    def hessian_parabola_2(x):
        return np.array([[4., 0.], [0., 3.]])

    x0_parabola_2 = np.array([-1., 1.5])
    mins_parabola_2 = [np.array([0., 0.]), ]


    def himmelblau(x):
        return (x[0]**2 + x[1] - 11.)**2 + (x[0] + x[1]**2 - 7.)**2

    def grad_himmelblau(x):
        return np.array([4.*x[0]*(x[0]**2 + x[1] - 11.) + 2.*(x[0] + x[1]**2 - 7.),
                         2.*(x[0]**2 + x[1] - 11.) + 4.*x[1]*(x[0] + x[1]**2 - 7.)])

    def hessian_himmelblau(x):
        return np.array([[4.*(x[0]**2 + x[1] - 11.) + 8.*x[0]**2 + 2., 4.*x[0] + 4.*x[1]],
                         [4.*x[0] + 4.*x[1], 2. + 4.*(x[0] + x[1]**2 - 7.) + 8.*x[1]**2]])

    x0_himmelblau = np.array([0, 0])
    mins_himmelblau = [np.array([3, 2]), np.array([-2.805118, 3.131312]),
                       np.array([-3.779310, -3.283186]), np.array([3.584428, -1.848126])]


    def mccormick(x):
        return np.sin(x[0] + x[1]) + (x[0] - x[1])**2 - 1.5 * x[0] + 2.5 * x[1] + 1.

    def grad_mccormick(x):
        return np.array([np.cos(x[0] + x[1]) + 2*(x[0] - x[1]) - 1.5,
                         np.cos(x[0] + x[1]) - 2*(x[0] - x[1]) + 2.5])

    def hessian_mccormick(x):
        return np.array([[-np.sin(x[0] + x[1]) + 2., -np.sin(x[0] + x[1]) - 2.],
                         [-np.sin(x[0] + x[1]) - 2., -np.sin(x[0] + x[1]) + 2.]])

    x0_mccormick = np.array([0, 2])
    mins_mccormick = [np.array([-0.54719, -1.54719]), ]


    functions = [(parabola_1, grad_parabola_1, hessian_parabola_1, x0_parabola_1, mins_parabola_1, "Parabola No. 1", 4),
                 (parabola_2, grad_parabola_2, hessian_parabola_2, x0_parabola_2, mins_parabola_2, "Parabola No. 2", 7),
                 (himmelblau, grad_himmelblau, hessian_himmelblau, x0_himmelblau, mins_himmelblau, "Himmelblau", 75),
                 (mccormick, grad_mccormick, hessian_mccormick, x0_mccormick, mins_mccormick, "McCormick", 5)]
    
    colors = [f'C{i}' for i in range(len(algorithms))]

    num_iterates = []

    for func, grad, hessian, x0, opt, func_title, L in functions:
        fig, axs = plt.subplots(2, 2)
        fig.suptitle(func_title)

        x = np.arange(-5, 5, 0.01)
        y = np.arange(-5, 5, 0.01)
        X, Y = np.meshgrid(x, y)
        Z = func(np.array([X, Y]))
        im = axs[0, 0].imshow(Z, vmin=Z.min(), vmax=Z.max(), origin='lower',
                              extent=[x.min(), x.max(), y.min(), y.max()])
        fig.colorbar(im, ax=axs[0, 0])

        all_iterates = []
        current_num_iterates = []

        for j, (alg, alg_title) in enumerate(algorithms):
            args = inspect.getfullargspec(alg).args
            args_dict = {'f': func, 'grad': grad, 'x0': x0}
            if 'L' in args:
                args_dict['L'] = L
            if 'hessian' in args:
                args_dict['hessian'] = hessian
            result = alg(**args_dict)
            iterates = np.array(result['iterates'])
            step_sizes = np.array(result['step_sizes'])
            gradient_norms = np.array(result['gradient_norms'])

            axs[0, 0].plot(iterates[:, 0], iterates[:, 1], '.-', label=alg_title, color=colors[j],loc='top')
            axs[0, 0].plot(iterates[-1, 0], iterates[-1, 1], '+', label="Opt. "+ alg_title)
            axs[0, 0].set_title("Optimization trajectory")
            axs[1, 0].plot(step_sizes, label=alg_title)
            axs[1, 0].set_title("Step size")
            axs[1, 0].legend()
            axs[0, 1].plot(gradient_norms, label=alg_title)
            axs[0, 1].set_title("Norm of the gradient")
            axs[0, 1].set_yscale('log')
            axs[0, 1].legend()

            all_iterates.append(iterates)
            current_num_iterates.append(len(iterates))

        num_iterates.append(current_num_iterates)

        for i, o in enumerate(opt):
            if i == 0:
                axs[0, 0].plot(o[0], o[1], '*', color='r', label="True optimum")
                #more visibility
                axs[0,0].xaxis.set_label_coords(.9, -.1)

            else:
                axs[0, 0].plot(o[0], o[1], '*', color='r')
        axs[0, 0].legend()

        def init():
            axs[1, 1].clear()

            axs[1, 1].imshow(Z, vmin=Z.min(), vmax=Z.max(), origin='lower',
                             extent=[x.min(), x.max(), y.min(), y.max()])

            for i, o in enumerate(opt):
                if i == 0:
                    axs[1, 1].plot(o[0], o[1], '*', color='r', label="True optimum")
                else:
                    axs[1, 1].plot(o[0], o[1], '*', color='r')

            for iterates in all_iterates:
                axs[1, 1].plot(iterates[0, 0], iterates[0, 1], '.-', label=alg_title, color=colors[j])

        def update(i):
            for j, iterates in enumerate(all_iterates):
                i = min(i, len(iterates)-1)
                axs[1, 1].plot(iterates[:i, 0], iterates[:i, 1], '.-', label=alg_title, color=colors[j])

            for j, o in enumerate(opt):
                if j == 0:
                    axs[1, 1].plot(o[0], o[1], '*', color='r', label="True optimum")
                else:
                    axs[1, 1].plot(o[0], o[1], '*', color='r')

        ani = animation.FuncAnimation(fig, update, frames=np.arange(max([len(iterates) for iterates in all_iterates])),
                                      init_func=init)
        plt.show()


    width = 0.5
    fig, ax = plt.subplots()
    for i, vals in enumerate(num_iterates):
        for j, (alg, alg_title) in enumerate(algorithms):
            if i == 0:
                ax.bar(i - width/2 + (j+.5)*width/len(algorithms),
                       vals[j], width/len(algorithms),
                       label=alg_title, color=colors[j])
            else:
                ax.bar(i - width/2 + (j+.5)*width/len(algorithms),
                       vals[j], width/len(algorithms),
                       color=colors[j])

    ax.set_ylabel("Number of iterations")
    ax.set_xlabel("Function")
    ax.set_title("Iterations until termination for the different functions and algorithms")
    ind = np.arange(len(num_iterates))
    func_titles = [func_title for _, _, _, _, _, func_title, _ in functions]
    ax.set_xticks(ind)
    ax.set_xticklabels(func_titles)
    ax.legend()

    plt.show()
